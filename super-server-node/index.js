const { ApolloServer } = require('apollo-server');

// 1
const typeDefs = `
  type Query {
    info: String!,
    allUsers: [User!]! 
  }

  type User {
    name: String!,
    surname: String!,
    phone: Int!
  }
`

let users = [{
 name: "human1",
 surname: "humanovOne",
 phone: 123213
},
{
  name: "human2",
  surname: "humanovTwo",
  phone: 123213
 }]

// 2
const resolvers = {
  Query: {
    info: () => `This is the API of a Hackernews Clone`,
    allUsers: () => users,
  }
}

// 3
const server = new ApolloServer({
  typeDefs,
  resolvers,
})

server
  .listen()
  .then(({ url }) =>
    console.log(`Server is running on ${url}`)
  );